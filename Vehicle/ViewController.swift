//
//  ViewController.swift
//  Floor is lava
//
//  Created by Rayan Slim on 2017-08-16.
//  Copyright © 2017 Rayan Slim. All rights reserved.
//

import UIKit
import ARKit
import CoreMotion
class ViewController: UIViewController, ARSCNViewDelegate {
    
    var add = UIButton()
    
    @IBOutlet weak var sceneView: ARSCNView!
    let configuration = ARWorldTrackingConfiguration()
    let motionManager = CMMotionManager()
    
    var vehicle = SCNPhysicsVehicle()
    var orientation: CGFloat = 0
    var touched:Int = 0
    var accelerationValues = [UIAccelerationValue(0), UIAccelerationValue(0)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sceneView.debugOptions = [ARSCNDebugOptions.showWorldOrigin, ARSCNDebugOptions.showFeaturePoints]
        self.configuration.planeDetection = .horizontal
        self.sceneView.session.run(configuration)
        self.sceneView.delegate = self
        self.setAcceleroMeter()
        self.addButton()
    }
    
    
    
    func addButton(){
        
        let borderAlpha: CGFloat = 0.7
        let cornerRadius: CGFloat = 5.0
        
        add = UIButton(frame: CGRect(x: 16, y: 589, width: 112, height: 46))
        add.backgroundColor = .clear
        add.layer.borderWidth = 1.0
        add.layer.borderColor = UIColor(white: 1.0, alpha: borderAlpha).cgColor
        add.layer.cornerRadius = cornerRadius
        add.setTitle("Add", for: .normal)
        
        add.addTarget(self, action: #selector(onClick), for: .touchUpInside)
        self.view.addSubview(add)
        
    }
    
    @objc func onClick(sender: UIButton!){
        self.addObject()
    }
    
    func addObject(){
        
        guard let pointOfView = sceneView.pointOfView else {return}
        let transform = pointOfView.transform
        let orientation = SCNVector3(-transform.m31, -transform.m32, -transform.m33)
        let location = SCNVector3(transform.m41, transform.m42, transform.m43)
        let currentPositionOfCamera = orientation + location
        
        let scene = SCNScene(named: "Car-Scene.scn")
        let chassis = (scene?.rootNode.childNode(withName: "chassis", recursively: false))!
        
        //Wheels
        let frontLeftWheel = chassis.childNode(withName: "frontLeftParent", recursively: false)
        let frontRightWheel = chassis.childNode(withName: "frontRightParent", recursively: false)
        let rearRightWheel = chassis.childNode(withName: "rearRightParent", recursively: false)
        let rearLeftWheel = chassis.childNode(withName: "rearLeftParent", recursively: false)
        
        let v_frontLeftWheel = SCNPhysicsVehicleWheel(node: frontLeftWheel!)
        let v_frontRightWheel = SCNPhysicsVehicleWheel(node: frontRightWheel!)
        let v_rearRightWheel = SCNPhysicsVehicleWheel(node: rearRightWheel!)
        let v_rearLeftWheel = SCNPhysicsVehicleWheel(node: rearLeftWheel!) 
        
        chassis.position = currentPositionOfCamera
        let body = SCNPhysicsBody(type: .dynamic, shape: SCNPhysicsShape(node: chassis, options: [SCNPhysicsShape.Option.keepAsCompound: true])) //gravity
        
        chassis.physicsBody = body
        //        body.mass = 5 //mass for the car
        
        self.vehicle = SCNPhysicsVehicle(chassisBody: chassis.physicsBody!, wheels: [v_rearRightWheel, v_rearLeftWheel, v_frontRightWheel, v_frontLeftWheel])
        self.sceneView.scene.physicsWorld.addBehavior(self.vehicle) //manage certain physics body works
        self.sceneView.scene.rootNode.addChildNode(chassis)
        
    }
    
    //ENGINE FORCE TO CONTROL CAR SPEED AND STEERING ANGLE
    func renderer(_ renderer: SCNSceneRenderer, didSimulatePhysicsAtTime time: TimeInterval) {
        
        var engineForce: CGFloat = 0
        var breakingForce: CGFloat = 0
        
        self.vehicle.setSteeringAngle(-orientation, forWheelAt: 2) //steer front right
        self.vehicle.setSteeringAngle(-orientation, forWheelAt: 3) //steer front left
        
        if self.touched == 1 {
            engineForce = 5
        } else if self.touched == 2 {
            engineForce = -5
        } else if self.touched == 3 {
            breakingForce = 100 //break
        } else {
            engineForce = 0
        }
        
        self.vehicle.applyEngineForce(engineForce, forWheelAt: 0)
        self.vehicle.applyEngineForce(engineForce, forWheelAt: 1)
        self.vehicle.applyBrakingForce(breakingForce, forWheelAt: 0)
        self.vehicle.applyBrakingForce(breakingForce, forWheelAt: 1)
        
    }
    
    //ACCELEROMETER
    func setAcceleroMeter(){
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 1/60
            motionManager.startAccelerometerUpdates(to: .main, withHandler: {(accelerometerData, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                
                self.accelerometerDidChange(acceleration: (accelerometerData!.acceleration))
            })
        } else {
            print("Accelerometer not available")
        }
        
    }
    
    //STEER DIRECTION
    
    func accelerometerDidChange(acceleration: CMAcceleration){
        accelerationValues[1] = filtered(currentAcceleration: accelerationValues[1], UpdateAcceleration: acceleration.y)
        accelerationValues[0] = filtered(currentAcceleration: accelerationValues[0], UpdateAcceleration: acceleration.x)
        
        if accelerationValues[0] > 0 {
            self.orientation = -CGFloat(accelerationValues[1])
        } else {
            self.orientation = CGFloat(accelerationValues[1])
        }
    }
    
    //FILTER ACCELERATION ( TO MAKE SMOOTHER STEER)
    func filtered(currentAcceleration: Double, UpdateAcceleration: Double) -> Double {
        let kfilteringFactor = 0.5
        return UpdateAcceleration * kfilteringFactor + currentAcceleration * (1-kfilteringFactor)
    }
    
    
    //INITIATE ENGINE / DRIVE
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let _ = touches.first else {return}
        self.touched += touches.count
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.touched = 0
    }
    
    
    func createConcrete(planeAnchor: ARPlaneAnchor) -> SCNNode {
        let concreteNode = SCNNode(geometry: SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(CGFloat(planeAnchor.extent.z))))
        concreteNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "concrete")
        concreteNode.geometry?.firstMaterial?.isDoubleSided = true
        concreteNode.position = SCNVector3(planeAnchor.center.x,planeAnchor.center.y,planeAnchor.center.z)
        concreteNode.eulerAngles = SCNVector3(90.degreesToRadians, 0, 0)
        
        let staticBody = SCNPhysicsBody.static() //fixtures
        concreteNode.physicsBody = staticBody //impact between surface and objects
        
        return concreteNode
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else {return}
        let concreteNode = createConcrete(planeAnchor: planeAnchor)
        node.addChildNode(concreteNode)
        print("new flat surface detected, new ARPlaneAnchor added")
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else {return}
        print("updating floor's anchor...")
        node.enumerateChildNodes { (childNode, _) in
            childNode.removeFromParentNode()
            
        }
        let concreteNode = createConcrete(planeAnchor: planeAnchor)
        node.addChildNode(concreteNode)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        guard let _ = anchor as? ARPlaneAnchor else {return}
        node.enumerateChildNodes { (childNode, _) in
            childNode.removeFromParentNode()
            
        }
        
    }
}


func +(left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x + right.x, left.y + right.y, left.z + right.z)
    
}
extension Int {
    
    var degreesToRadians: Double { return Double(self) * .pi/180}
}

